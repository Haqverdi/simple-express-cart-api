// import lodash for manupulating with objects and arrays
const _ = require('lodash');

// test products
const products = {
    1: {
        "id": 1,
        "title": "iPad 4 Mini",
        "price": 500.00,
        "description": "Product Description",
        "image": "./test/ipad-mini.png"
    },
    2: {
        "id": 2,
        "title": "H&M T-Shirt White",
        "price": 10.00,
        "description": "Product Description",
        "image": "./test/t-shirt.png"
    },
    3: {
        "id": 3,
        "title": "Charli XCX - Sucker CD",
        "price": 20.00,
        "description": "Product Description",
        "image": "./test/sucker.png"
    }
}
// test cart
let cart = {
    "total_sum": 0,
    "products_count": 0,
    "products": {
    }
};

// Getting a list of products
exports.getAllItems = () => {
    return new Promise((resolve, reject) => {
        // if no items
        if (products.length == 0) reject('продукта нет в системе');
        // else send list of items
        resolve(products);
    });
}
// Add item to cart by id and count, and update cart
exports.addItemToCart = (product_id, quantity) => {
    return new Promise((resolve, reject) => {
        // check for item in products list by id
        const item = products[product_id];
        if (item == undefined) reject('Такого продукта нет в системе');
        let cart_item = cart.products[product_id];
        // check for item in cart by id
        // if item in cart, increase item count and update cart
        if (cart_item != undefined) {
            cart_item.quantity = cart_item.quantity + quantity;
            cart_item.sum = item.price * cart_item.quantity;
            cart.total_sum += item.price * quantity;
            cart.products_count += quantity;
            cart.products = { ...cart.products, [cart_item.id]: cart_item };
            resolve(cart);
        // else add item and update cart
        } else {
            let new_item = {};
            new_item.id = item.id;
            new_item.quantity = quantity;
            new_item.sum = item.price * quantity;
            cart.total_sum += item.price * quantity;
            cart.products_count += quantity;
            cart.products = { ...cart.products, [new_item.id]: new_item };
            resolve(cart);
        }
    });
}
// delete item from cart by id and update cart
exports.deleteItem = (product_id) => {
    return new Promise((resolve, reject) => {
        // check for item by id
        const item = products[product_id];
        // check for item in cart
        let cart_item = cart.products[product_id];
        // if not found item in cart
        if (cart_item == undefined) reject('Такого продукта нет в системе');
        // if item left only 1, delete item from cart and update cart
        if (cart_item.quantity == 1) {
            cart.total_sum -= cart_item.sum;
            cart.products_count -= 1;
            cart.products = _.omit(cart.products, product_id);
            resolve(cart);
        // else reduce item count in cart and update cart
        } else {
            cart.total_sum -= item.price;
            cart.products_count -= 1;
            cart.products = { ...cart.products, [cart_item.id]: cart_item };
            cart_item.quantity = cart_item.quantity - 1;
            cart_item.sum = item.price * cart_item.quantity;
            resolve(cart);
        }
    });
}
// get cart info
exports.getCart = () => {
    return new Promise((resolve, reject) => {
        // if cart not found, send error msg
        if (cart == undefined) reject('Cart not found');
        // else send cart
        resolve(cart);
    });
}