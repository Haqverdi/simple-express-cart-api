const app = require('../app');
const should = require('should');
const supertest = require('supertest');

describe('cart-operations-test', () => {

    // test for get list of products
    it('should return list all products', (done) => {
        supertest(app)
        .get('/api/products')
        .expect(200)
        .end((err, res) => {
            if (err) throw err;
            res.status.should.equal(200);
            done();
        });
    });
    // test for get cart
    it('should return list of all products in cart', (done) => {
        supertest(app)
        .get('/api/cart')
        .expect(200)
        .end((err, res) => {
            if (err) throw err;
            res.status.should.equal(200);
            done();
        });
    });
    // test for add product
    it('should add items to cart by id and count', (done) => {
        supertest(app)
        .post('/api/cart?product_id=1&quantity=1')
        .expect(200)
        .end((err, res) => {
            if (err) throw err;
            res.status.should.equal(200);
            res.body.status.should.equal('ok');
            done();
        });
    });
    // test for add product *with wrong params
    it('should return error with status 400', (done) => {
        supertest(app)
        .post('/api/cart?product_id=1&quantity=11')
        .expect(400)
        .end((err, res) => {
            if (err) throw err;
            res.status.should.equal(400);
            done();
        });
    });
    // test for delete product from cart
    it('should delete item(s) from cart by id', (done) => {
        supertest(app)
        .delete('/api/cart?product_id=1')
        .expect(200)
        .end((err, res) => {
            if (err) throw err;
            res.status.should.equal(200);
            res.body.status.should.equal('ok');
            done();
        });
    });
    //test for delete product from cart with *wrong params
    it('should return error error with wrong id', (done) => {
        supertest(app)
        .delete('/api/cart?product_id=11')
        .expect(500)
        .end((err, res) => {
            if (err) throw err;
            res.status.should.equal(500);
            done();
        });
    });

})