# Simple Express Cart API

> A simple cart API built on top Node.js, Express and etc.

## Features

- No transpilers, just vanilla javascript (ES6)
- Request validation with express-validator
- Gzip compression
- Tests with Mocha
- Logging with Morgan
- Body Parsing via body-parser
- and etc.

## Requirements

- [Node](https://nodejs.org/en/download/current/)

## Getting Started
***

Clone the repo and make it yours:

```bash
git clone https://Haqverdi@bitbucket.org/Haqverdi/test-cart.git
cd test-cart
rm -rf .git
```

Install dependencies:

```bash
npm insatall
```

Run in development mode with nodemon

```bash
npm run dev
```

Run in production mode

```bash
npm run start
```

Run all tests with Mocha

```bash
npm test
```

## Endpoints
***
> in the test purposes in the model, as a database the object is used

This endpoint used add a product to cart.

```
- [POST] - /api/cart?product_id=*&quantity=*
```

This endpoint used delete a product from cart.

```
- [DELETE] - /api/cart?product_id=*
```

This endpoint used to get cart info.

```
- [GET] - /api/cart
```

This endpoint used to get list of products.
```
- [GET] - /api/products
```


## LICENSE


MIT © Haqverdi Behbudov