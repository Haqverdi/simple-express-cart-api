const express = require('express');
const router = express.Router();
// get all controllers
const {
	getAllProducts,
	getCart,
	addItem,
	deleteItem
} = require('../controllers/index');

// Getting a list of products
router.get('/api/products', getAllProducts);

// Retrieving shopping cart information
router.get('/api/cart', getCart);

// Add product to shopping cart (must have product_id, quantity as param)
router.post('/api/cart', addItem);

// Deleting a product from the shopping cart (must have product_id as param)
router.delete('/api/cart', deleteItem);

module.exports = router;