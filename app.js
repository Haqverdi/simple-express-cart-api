const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const compression = require('compression');
const validator = require('express-validator');

// Create the Express application object
const app = express();


// express middlewares
app.use(helmet());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(compression());
app.use(validator());

// routes
app.use('/', require('./routes/index'));

// Catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error();
    err.status = 404;
    err.type = 'invalid_request_error';
    err.message = `Unable to resolve the request '${req.url}'`;
    next(err);
});

// error handler
app.use((err, req, res, next) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // send error status
    res.status(err.status || 500).json({error: err});
});

module.exports = app;
