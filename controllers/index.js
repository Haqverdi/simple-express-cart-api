const {
    getAllItems,
    addItemToCart,
    deleteItem,
    getCart
} = require('../models/index');

// controllers
module.exports = {
    // get list of products
    getAllProducts: (req, res) => {
        getAllItems()
        .then((result) => {
            res.status(200).json({result});
        }).catch((err) => {
            res.status(500).json({err})
        });
    },
    // get cart
    getCart: (req, res) => {
        getCart()
        .then((result) => {
            res.status(200).json({result});
        }).catch((err) => {
            res.status(500).json({err});
        });
    },
    // add item (post)
    addItem: (req, res) => {
        // params validations
        req.checkQuery('product_id')
        .matches(/^\d+$/).withMessage('must contain a number')
        .notEmpty().trim().exists().withMessage('must contain id').escape().toInt();
        req.checkQuery('quantity')
        .notEmpty().trim().exists().escape().isInt({lt: 11}).withMessage('must less than and equal 10')
        .matches(/^\d+$/).withMessage('must contain a number').toInt();
        // check for validation error
        req.getValidationResult().then((errors) => {
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }
            // if no error with params add item
            addItemToCart(req.query.product_id, req.query.quantity)
            .then((result) => {
                res.status(200).json({status: 'ok', result});
            }).catch((err) => {
                res.status(500).json({err});
            });
        });
    },
    // delte item from cart (delete)
    deleteItem: (req, res) => {
        // param validation
        req.checkQuery('product_id')
        .notEmpty().trim().exists().withMessage('must contain id').escape()
        .matches(/^\d+$/).withMessage('must contain a number').toInt();
        // check for param validation errors
        req.getValidationResult().then((errors) => {
            if (!errors.isEmpty()) {
                return res.status(400).send({ errors: errors.array() });
            }
            // if no error with item id, delete from cart and return status
            deleteItem(parseInt(req.query.product_id))
            .then(() => {
                res.status(200).json({status: 'ok'});
            }).catch((err) => {
                res.status(500).json({err});
            });
        });
    }
}